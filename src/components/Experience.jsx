import placeholder from '../assets/intro.png'
import js from '../assets/javascript.png'
import ts from '../assets/typescript.png'
import react from '../assets/react.png'
import Cplus from '../assets/Cplus.png'
import nextjs from '../assets/nextjs.svg'
import tailwind from '../assets/tailwind.svg'

const Experience = () => {

	const techs = [
		{
			id:1,
			src: js,
			title:'JavaScript',
			style:'shadow-yellow-500'
		},
		{
			id:2,
			src: ts,
			title:'Typescript',
			style:'shadow-blue-500'
		},
		{
			id:3,
			src: Cplus,
			title:'C++',
			style:'shadow-blue-500'
		},
		{
			id:4,
			src:  react,
			title:'ReactJS',
			style:'shadow-blue-400'
		},
		{
			id:5,
			src: nextjs,
			title:'NextJS',
			style:'shadow-white'
		},
		{
			id:6,
			src: tailwind,
			title:'Tailwind',
			style:'shadow-cyan-200'
		}
	]

  return (
	<div  name="experience" className='bg-gradient-to-b from-gray-800 to-gray-900 w-full text-white'>
		<div className='max-w-screen-lg p-4 pt-20  mx-auto flex flex-col justify-center items-center w-full h-full'>
			<div className='pb-8 text-center'>
				<h4  className="text-4xl font-bold inline border-b-4 border-gray-500">Tech Skills</h4>
				<p className='py-6'>These are the technologies I`ve worked with</p>
			</div>
			<div className='grid grid-cols-3 sm:grid-cols-6 gap-8 px-12 sm:px-0 '>
				{techs.map( ({id, src, title, style }) => (
				<div key={id} className={`shadow-md py-2 rounded-lg hover:scale-105 duration-500
				${style}`}>
					<img src={src} alt={title} className='w-[50%] mx-auto my-2'/>
					<p className='mt-4 text-center'>{title}</p>
				</div>
				))}
			</div>
		</div>
	</div>
  )
}

export default Experience