

const About = () => {
	return (
	<div name="about" className="w-full  bg-gradient-to-b from-gray-800 to-gray-900
		text-white">
		<div className="max-w-screen-lg p-10 pt-0 mx-auto flex flex-col justify-center w-full">
			<div className="pb-8 text-center">
				<h4 className="text-4xl font-bold inline border-b-4 border-gray-500">
					About Me
				</h4>
			</div>
			<p className="px-5 sm:text-xl text-gray-300 ">
				As a Junior Software Developer, I sharpened my coding skills in a project-based, peer-to-peer learning environment 
				at the coding school <a href="https://42wolfsburg.de/" className="underline hover:text-red-700">42 Wolfsburg</a>. 
				In September 23, I finsihed advanced training in Full Stack Web-and App Development at <a href="https://www.wbscodingschool.com/"  className="underline hover:text-red-700">WBS Coding School</a>. 
				I have practical experience in working with JavaScript, Typescript, React, C++, C. 
				My strong communication skills and proven ability to collaborate effectively in team settings further 
				enhance my professional profile.
			</p>
		</div>
	</div>
	)
}

export default About