
const Contact = () => {
  return (
	<div name="contact"
	className='bg-gradient-to-b from-gray-900 to-gray-800 w-full text-white'>
		<div className='max-w-screen-lg p-4 pt-20  mx-auto flex flex-col justify-center items-center w-full h-full'>
			<div className="pb-8 text-center">
				<h4  className="text-4xl font-bold inline border-b-4 border-gray-500">Contact</h4>
				<p className='py-6'>Get in Touch with Me </p>
			</div>

			<div className="flex justify-center items-center w-full">
				<form action="https://getform.io/f/e7f91dd2-4720-4796-a45a-a645be42dab5" method="POST"
				className="flex flex-col w-80">
					<input type="text" name="name" placeholder="Enter Your Name"
					className="p-2 m-1  bg-transparent rounded-md border-2 focus:outline-none text-white"/>
					<input type="text" name="email" placeholder="Enter Your Email"
						className="p-2 m-1 bg-transparent rounded-md border-2  focus:outline-none text-white"/>
					<textarea name="message" rows="10"
					className="p-2 m-1 bg-transparent border-2 rounded-md focus:outline-none text-white"
					></textarea>
					<button 
					className="text-white bg-gradient-to-b from-cyan-500 to-cyan-800
						py-3 px-10 my-8 mx-auto rounded-md hover:scale-110 
						duration-300">
						Send
					</button>
				</form>
			</div>
		</div>
	</div>
  )
}

export default Contact