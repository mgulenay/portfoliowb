import {FaGithub, FaLinkedin} from 'react-icons/fa';
import {HiOutlineMail} from 'react-icons/hi';
//import { BsFillPersonalLinesFill } from 'react-icons/bs';

const SocialLinks = () => {


  return (
	<div className='hidden md:flex flex-col top-[35%] left-0 fixed '>
		<ul>
		{[
			[1, 'Linkedin', 'https://www.linkedin.com/in/mervegulenay/', <FaLinkedin key={1} size={30} />],
			[2, 'Github', 'https://github.com/GulenayMer', <FaGithub key={2} size={30} />],
			[3, 'Email', '', <HiOutlineMail key={3} size={30}/>],
			].map(([id, title, href, icon]) => (
			<li key={id}  className='flex justify-between items-center w-40 
				h-14 px-4 ml-[-100px] bg-gray-100 hover:ml-[-10px]
				hover:rounded-md duration-300'>
				<a href={href} target="_blank" rel="noopener noreferrer" 
					className='flex justify-between items-center w-full'>
				<>
					{title}
					{icon}
				</>	
				</a>
			</li>
		))}
		
		</ul>
	</div>
  )
}

export default SocialLinks