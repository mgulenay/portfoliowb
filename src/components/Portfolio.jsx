import placeholder from '../assets/intro.png';
import moviesApp from '../assets/moviesApp.jpg';
import cryptoUpdate from '../assets/cryptoUpdate.jpg';
import lyricsSearch from '../assets/lyricsSearch.jpg';
import breakoutGame from '../assets/breakoutGame.jpg';
import expenseTracker from '../assets/expenseTracker.jpg';
import mealFinder from '../assets/mealFinder.jpg';


// TODO: add a demo, code link in the array --, create onclick function &  use indexes 
// to open the links
/*
	MovieApp (PERN Stack)
	CryptoCurrency App (ReactJS)
	Lyric search thing from Vanilla JS(CSS, JS, HTML), BreakOut Game (), Infinite Scroll, ExpenseTracker
	Events App (MERN STack)
	Authentication with NestJS
*/ 
/* Maybe a few group projects?
	Cub3D(C)
	WebServer(C++)
	Transend.(Typescript, React)
*/
const Portfolio = () => {

	const projects = [
		{
			id:1,
			src:moviesApp,
			demo: 'https://quiet-shortbread-7f5636.netlify.app/',
			code: 'https://github.com/GulenayMer/movieApp/'
		},
		{
			id:2,
			src:cryptoUpdate,
			demo:'https://shimmering-crostata-a7ee78.netlify.app/',
			code:'https://github.com/GulenayMer/CryptoUpdate'
		},
		{
			id:3,
			src:lyricsSearch,
			demo:'https://fancy-pony-74c074.netlify.app/',
			code:'https://gitlab.com/mgulenaythedevoloper/lyricsearch'
		},
		{
			id:4,
			src:breakoutGame,
			demo:'',
			code:''
		},
		{
			id:5,
			src:expenseTracker,
			demo:'',
			code:''
		},
		{
			id:6,
			src:mealFinder,
			demo:'',
			code:''
		}
	]

  return (
	<div name="projects" className='bg-gradient-to-b from-gray-900 to-gray-800 w-full text-white'>
		<div className='max-w-screen-lg p-4 pt-20 mx-auto flex flex-col 
						justify-center items-center w-full h-full'>
			<div className='pb-8 text-center'>
				<h4  className="text-4xl font-bold inline border-b-4 border-gray-500">Portfolio</h4>
				<p className='py-6'>Check Out Some of My Projects</p>
			</div>

			<div className='grid sm:grid-cols-2 md:grid-cols-3 gap-8 px-12 sm:px-0'>
			{
				projects.map( ({id, src, demo, code}) => (
					<div key={id} className='shadow-md shadow-gray-600 rounded-lg'>
					<img src={src} alt="img"
					className='rounded-md duration-200 hover:scale-105 w-full h-[70%] object-cover'
					/>
					<div className='flex items-center justify-center'>
						<a href={demo} target='blank'>
						<button className='w-1/2 px-6 py-3 m-4 duration-200 hover:scale-105'>
							Demo
						</button>
						</a>
						<a href={code} target='blank'>
						<button className='w-1/2 px-6 py-3 m-4 duration-200 hover:scale-105'>
							Code
						</button>
						</a>
						
					</div>
				</div>
				))
			}
		
			</div>
		</div>
	</div>
	)
}

export default Portfolio