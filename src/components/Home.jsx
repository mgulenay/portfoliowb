import HeroImage from '../assets/myprofile.jpg';
import {HiOutlineArrowNarrowRight} from 'react-icons/hi'
import {Link} from 'react-scroll'

const Home = () => {
	return (
		<div name='home' 
			className='w-full md:h-screen p-10 bg-gradient-to-b from-gray-900 to-gray-800'>
			<div className='max-w-screen-lg mx-auto flex flex-col justify-center
			items-center h-full px-4 text-white my-4 md:flex-row'>
				<div>
					<img src={HeroImage} alt="my pic"
					className='rounded-2xl mt-20 w-1/3 mx-auto md:w-[60%] md:mt-0'/>
				</div>
				<div className='flex flex-col justify-center p-20 md:p-0 h-full '>
					<h2 className='text-4xl 2xl:text-6xl font-bold'>
						Hi, I am Merve
					</h2>
					<p className='text-gray-100 py-4'>
						I am deeply passionate in all facets of software development, with a particular interest in
						Frontend, Backend and Full Stack Development. 
						Currently, I enjoy working on web applications using technologies like ReactJS, NextJS, Typescript and NodeJS
						to create dynamic and immersive digital experiences.
					</p>
					<div>
						<Link to="projects" smooth duration={500} 
						className='group w-fit px-6 py-3 my-4 flex items-center text-black
							rounded-md bg-gradient-to-r from-gray-100 to-gray-200 hover:bg-gradient-to-br cursor-pointer'>
							Portfolio
							<span className='ml-1 group-hover:rotate-90 duration-300'>
								<HiOutlineArrowNarrowRight/>
							</span>						
						</Link>
					</div>
				</div>

			</div>




		</div>
	)
}

export default Home