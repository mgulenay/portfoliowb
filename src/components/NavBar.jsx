import { useState } from 'react'
import { FaBars, FaTimes } from 'react-icons/fa'
import {Link} from 'react-scroll'

const NavBar = () => {

	const [toggle, setToggle] = useState(false);

	
	return (
	<div className='flex justify-between items-center w-full h-20
		bg-gradient-to-b from-gray-800 to-gray-900 text-white px-4 fixed'>
		<div>
			<h1 className='text-4xl font-signature ml-2'>MG</h1>
		</div>
		<ul className='hidden md:flex'>
		{[
			[1, 'Home', 'home'],
			[2, 'About', 'about'],
			[3, 'Projects', 'projects'],
			[4, 'Experience', 'experience'],
			[5, 'Contact', 'contact'],
			].map(([id, title, name]) => (
			<li key={id} className="px-4 cursor-pointer hover:scale-105 duration-200">
				<Link to={name} smooth duration={500}>{title}</Link>
			</li>	
		))}
		</ul>

		<div className='cursor-pointer pr-4 z-10 md:hidden'
			onClick={ () => setToggle(!toggle)}>
			{toggle ? <FaTimes size={30}/> : <FaBars size={30}/> }
		</div>

		{toggle && (
			<ul className='flex flex-col justify-center items-center absolute 
						top-0 left-0 w-full h-screen bg-gradient-to-b from-black to-gray-800'>
		{[
			[1, 'Home', 'home'],
			[2, 'About', 'about'],
			[3, 'Projects', 'projects'],
			[4, 'Experience', 'experience'],
			[5, 'Contact', 'contact'],
			].map(([id, title, name]) => (
			<li key={id} className="px-4 py-5 text-3xl text-gray-400 cursor-pointer hover:scale-105 duration-200">
				<Link onClick={() => setToggle(!toggle)} to={name} smooth duration={500}>{title}</Link>
			</li>	
		))}

		</ul>
		)}
	</div>
	)
}

export default NavBar